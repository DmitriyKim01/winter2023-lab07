public class Board{
	private Square[][] tictactoeBoard;
	
	public Board(int n){
		tictactoeBoard = new Square[n][n];
		for (int i = 0; i < tictactoeBoard.length; i++){
			for (int j = 0; j < tictactoeBoard[i].length; j++){
				tictactoeBoard[i][j] = Square.BLANK;
			}
		}
	}
	
	public String toString(){
		String result = "";
		String horizontalCount = "  ";
		for (int i = 0; i < tictactoeBoard.length; i++){
			result += i+1 + " ";
			for (int j = 0; j < tictactoeBoard[i].length; j++){
				result += tictactoeBoard[i][j].toString()+ " ";
			}
			result += "\n";
			horizontalCount += i+1 + " ";
		}
		result = horizontalCount + "\n" + result;
		return result;
	}
	
	public boolean placeToken(int row, int col, Square playerToken){
		if (row > tictactoeBoard.length-1 || col > tictactoeBoard.length-1 || row < 0 || col < 0){
			return false;
		}
		if (tictactoeBoard[col][row] == Square.BLANK){
			tictactoeBoard[col][row] = playerToken;
			return true;
		}
		return false;
	}
	
	public boolean checkIfFull(){
		for (int i = 0; i < tictactoeBoard.length; i++){
			for (int j = 0; j < tictactoeBoard[i].length; j++){
				if (tictactoeBoard[i][j] == Square.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		for (int i = 0; i < tictactoeBoard.length; i++){
			int countWins = 0;
			for (int j = 0; j < tictactoeBoard[i].length; j++){
				if (tictactoeBoard[i][j] == playerToken){
					countWins++;
				}
			}
			if (countWins == tictactoeBoard.length){
				return true;
			}
		}
		return false;
	}
	
	private boolean checkIfWinningVertical(Square playerToken){
		for (int i = 0; i < tictactoeBoard.length; i++){
			int countWins = 0;
			for (int j = 0; j < tictactoeBoard[i].length; j++){
				if (tictactoeBoard[j][i] == playerToken){
					countWins++;
				}
			}
			if (countWins == tictactoeBoard.length){
				return true;
			}
		}
		return false;
	}
	
	private boolean checkIfWinningDiagonal(Square playerToken){
		int countLeftToRight = 0;
		int countRightToLeft = 0;
			for (int j = 0; j < tictactoeBoard.length; j++){
				if (tictactoeBoard[j][j] == playerToken){
					countLeftToRight++;
				}
				if (tictactoeBoard[0+j][tictactoeBoard.length-1-j] == playerToken){
					countRightToLeft++;
				}
			}
			if (countLeftToRight == tictactoeBoard.length || countRightToLeft == tictactoeBoard.length){
				return true;
			}
			return false;
	}
	
	public boolean checkIfWinning(Square playerToken){
		if (checkIfWinningVertical(playerToken) || checkIfWinningHorizontal(playerToken) || checkIfWinningDiagonal(playerToken)){
			return true;
		}
		return false;
	}
}
import java.util.Scanner;

public class TicTacToe{
	// Global variables:
	static Scanner scan = new Scanner(System.in);
	static int winsPlayerX = 0;
	static int winsPlayerO = 0;
	
	// Main method:
	public static void main(String[] args){
		char continueButton = ' '; 
		do{
			System.out.println("Welcome on my super fancy game!\n");
			mainGameLoop();
			System.out.println("Would you like to continue?");
			System.out.println("Press <q> to quit");
			continueButton = scan.next().charAt(0);
		}while(continueButton != 'q');
		System.out.println("Player X wins " + winsPlayerX + " times");
		System.out.println("Player O wins " + winsPlayerO + " times");
	}
	// Game loop:
	public static void mainGameLoop(){
		Board board = defineBoard();
		boolean gameOver = false;
		// Changed to 2, because playerX always starts first:
		int player = 2;
		Square playerToken = Square.X;
		do{
			System.out.println(board);
			if (player == 1){
				playerToken = Square.O;
			}
			if (player == 2){
				playerToken = Square.X;
			}
			
			System.out.println("Player " + playerToken);
			System.out.print("Row: ");
			int row = scan.nextInt();
			System.out.print("Column: ");
			int col = scan.nextInt();
			// Validate the input:
			while(!board.placeToken(col-1, row-1, playerToken)){
				System.out.println("You entered wrong position or the place is already taken!");
				System.out.println("Player " + playerToken);
				System.out.print("Row: ");
				row = scan.nextInt();
				System.out.println();
				System.out.print("Column: ");
				col = scan.nextInt();
			}
			// Check for winner:
			if (board.checkIfWinning(playerToken)){
				System.out.println("Player " + playerToken + " won the game!");
				if (playerToken == Square.O){
					winsPlayerO++;
				}
				if(playerToken == Square.X){
					winsPlayerX++;
				}
				gameOver = true;
				System.out.println(board);
			}
			else if (board.checkIfFull()){
				System.out.println("It's a tie!");
				gameOver = true;
				System.out.println(board);
			}
			else{
				player++;
				if (player > 2){
					player = 1;
				}
			}
		}while(!gameOver);
	}

	public static Board defineBoard(){
		System.out.println("Enter the size of the board (3-9): ");
		int size = scan.nextInt();
		while(size > 9 || size < 3){
			System.out.println("you entered wrong input!");
			size = scan.nextInt();
		}
		Board board = new Board(size);
		return board;
	}
	
}